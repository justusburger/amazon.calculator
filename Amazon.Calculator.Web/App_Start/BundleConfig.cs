﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Amazon.Calculator.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;
            
            bundles.Add(new ScriptBundle("~/bundles/libs")
                .Include("~/Scripts/libs/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom")
                .Include(
                "~/Scripts/Calculation.js",
                "~/Scripts/Answer.js",
                "~/Scripts/CalculationFactory.js",
                "~/Scripts/Calculator.js",
                "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/bundles/css")
                .IncludeDirectory("~/Content/css/", "*.css"));

        }
    }
}