﻿$(function() {
    $('#evaluate').on('click', function () {
        var calculationLineSet = $('#input').val().match(/^[0-9]+#-(SUM|AVERAGE|MIN|MAX):[\s]*([0-9\s,]*)+$/gim);
        var calculationSet = [];
        $(calculationLineSet).each(function (i, line) {
            var calculation = CalculationFactory.ParseCalculation(line.trim());
            calculationSet.push(calculation);
        });
        var answerSet = [];
        $(calculationSet).each(function (i, calculation) {
            var answer = Calculator.Calculate(calculation);
            answerSet.push(answer);
        });
        var result = '';
        $(answerSet).each(function (i, answer) {
            result += (i + 1) + answer.toString() + "\n";
        });
        $('#result').val(result);
    });
});