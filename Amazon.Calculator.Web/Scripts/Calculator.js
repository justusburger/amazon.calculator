﻿var Calculator = (function ($, Answer) {
    var self = { };
    self.Calculate = function (calculation) {
        if (calculation.Operator === undefined || calculation.Operator.length < 1)
            throw 'Calculation operator not found.';
        if (calculation.OperandSet === undefined || calculation.OperandSet.length === 0)
            throw 'Calculation operand set not found or empty.';

        var result = null;
        var operator = calculation.Operator.toUpperCase();
        switch (operator) {
            case 'MIN':
                result = self.Min(calculation.OperandSet);
                break;
            case 'MAX':
                result = self.Max(calculation.OperandSet);
                break;
            case 'AVERAGE':
                result = self.Average(calculation.OperandSet);
                break;
            case 'SUM':
                result = self.Sum(calculation.OperandSet);
                break;
        }

        if (result === null)
            throw 'Unknown calculation operator.';
        else {
            var answer = new Answer(operator, result);
            return answer;
        }
    };
    self.Min = function (operandSet) {
        if (operandSet === undefined || operandSet.length < 1)
            throw 'Calculation operand set not found or empty.';

        var lastKnownMin = Number.MAX_VALUE;
        $(operandSet).each(function (i, operand) {
            if (operand < lastKnownMin)
                lastKnownMin = operand;
        });
        return lastKnownMin;
    };
    self.Max = function (operandSet) {
        if (operandSet === undefined || operandSet.length < 1)
            throw 'Calculation operand set not found or empty.';

        var lastKnownMax = Number.MIN_VALUE;
        $(operandSet).each(function (i, operand) {
            if (operand > lastKnownMax)
                lastKnownMax = operand;
        });
        return lastKnownMax;
    };
    self.Average = function (operandSet) {
        if (operandSet === undefined || operandSet.length < 1)
            throw 'Calculation operand set not found or empty.';

        var operandSetSum = self.Sum(operandSet);
        var average = operandSetSum / operandSet.length;
        return average;
    };
    self.Sum = function (operandSet) {
        if (operandSet === undefined || operandSet.length === 0)
            throw 'Calculation operand set not found or empty.';

        var lastKnownSum = 0;
        $(operandSet).each(function (i, operand) {
            lastKnownSum += operand;
        });
        return lastKnownSum;
    };
    return self;
})(jQuery, Answer);