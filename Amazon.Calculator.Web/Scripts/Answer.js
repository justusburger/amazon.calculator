﻿var Answer = (function () {
    var toString = function () {
        return '#:' + this.Operator + '=' + this.Result;
    };
    return function (operator, result) {
        return {
            Operator: operator,
            Result: result,
            toString: toString
        };
    };
})();