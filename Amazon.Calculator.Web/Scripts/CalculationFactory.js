﻿var CalculationFactory = (function ($, Calculation) {
    var parse = function (line) {
        if (line === undefined)
            throw 'Could not parse line. Line not supplied.';

        var operator = line.substring(line.indexOf('-') + 1, line.indexOf(':'));
        var operandStringSet = line.substring(line.indexOf(':') + 1).match(/[0-9]+/gi);
        var operandSet = [];
        $(operandStringSet).each(function (i, operandString) {
            operandSet.push(parseFloat(operandString));
        });
        var calculation = new Calculation(operator, operandSet);
        return calculation;
    };

    return {
        ParseCalculation: parse
    };
})(jQuery, Calculation);