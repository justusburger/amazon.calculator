﻿$(function() {

    test('Calculation - Constructor sets object properties.', function() {
        var calc = new Calculation('SUM', [1, 4, 5]);
        equal(calc.Operator, 'SUM');
        equal(calc.OperandSet.length, 3);
        equal(calc.OperandSet[0], 1);
        equal(calc.OperandSet[1], 4);
        equal(calc.OperandSet[2], 5);
    });

    test('Answer - Constructor sets object properties.', function() {
        var answer = new Answer('SUM', 12);
        equal(answer.Operator, 'SUM');
        equal(answer.Result, 12);
    });

    test('Answer.toString returns expected results.', function() {
        var answer = new Answer('SUM', 11);
        equal(answer.toString(), '#:SUM=11');
    });

    test('CalculationFactory.ParseCalculation - throws exception if line not supplied.', function() {
        throws(function() {
            CalculationFactory.ParseCalculation();
        }, /Could not parse line. Line not supplied./);
    });

    test('CalculationFactory.ParseCalculation - parses line and constructs correct calculation object.', function() {
        var calculation = CalculationFactory.ParseCalculation('1#-SUM:3,5,7');
        equal(calculation.Operator, 'SUM');
        equal(calculation.OperandSet.length, 3);
        equal(calculation.OperandSet[0], 3);
        equal(calculation.OperandSet[1], 5);
        equal(calculation.OperandSet[2], 7);
    });

    test('Calculator.Calculate - Throws exception if operator not supplied.', function() {
        throws(function() {
            Calculator.Calculate({ OperandSet: [1, 2, 3] });
        }, /Calculation operator not found./);
    });

    test('Calculator.Calculate - Throws exception if operandSet not supplied.', function() {
        throws(function() {
            Calculator.Calculate({ Operator: 'SUM' });
        }, /Calculation operand set not found or empty./);
    });

    test('Calculator.Calculate - Calculation with MIN operator calls min function.', function() {
        Calculator._minTimesExecuted = 0;
        Calculator._Min = Calculator.Min;
        Calculator.Min = function() {
            Calculator._minTimesExecuted++;
        };
        var answer = Calculator.Calculate(new Calculation('MIN', [1, 3, 4]));
        equal(Calculator._minTimesExecuted, 1);
        Calculator.Min = Calculator._Min;
        Calculator._minTimesExecuted = undefined;
    });

    test('Calculator.Calculate - Calculation with MAX operator calls max function.', function() {
        Calculator._maxTimesExecuted = 0;
        Calculator._Max = Calculator.Max;
        Calculator.Max = function () {
            Calculator._maxTimesExecuted++;
        };
        var answer = Calculator.Calculate(new Calculation('MAX', [1, 3, 4]));
        equal(Calculator._maxTimesExecuted, 1);
        Calculator.Max = Calculator._Max;
        Calculator._maxTimesExecuted = undefined;
    });

    test('Calculator.Calculate - Calculation with AVERAGE operator calls average function.', function() {
        Calculator._averageTimesExecuted = 0;
        Calculator._Average = Calculator.Average;
        Calculator.Average = function () {
            Calculator._averageTimesExecuted++;
        };
        var answer = Calculator.Calculate(new Calculation('AVERAGE', [1, 3, 4]));
        equal(Calculator._averageTimesExecuted, 1);
        Calculator.Average = Calculator._Average;
        Calculator._averageTimesExecuted = undefined;
    });

    test('Calculator.Calculate - Calculation with SUM operator calls sum function.', function() {
        Calculator._sumTimesExecuted = 0;
        Calculator._Sum = Calculator.Sum;
        Calculator.Sum = function () {
            Calculator._sumTimesExecuted++;
        };
        var answer = Calculator.Calculate(new Calculation('SUM', [1, 3, 4]));
        equal(Calculator._sumTimesExecuted, 1);
        Calculator.Sum = Calculator._Sum;
        Calculator._sumTimesExecuted = undefined;
    });

    test('Calculator.Calculate - Calculation throws exception is operator not found.', function() {
        throws(function() {
            var answer = Calculator.Calculate(new Calculation('aaa', [1, 3, 4]));
        }, /Unknown calculation operator./);
    });

    test('Calculator.Calculate - Min calculates min value.', function() {
        var result = Calculator.Min([1, 3, 4]);
        ok(result == 1);
    });

    test('Calculator.Calculate - Max calculates min value.', function() {
        var result = Calculator.Max([1, 3, 4]);
        ok(result == 4);
    });

    test('Calculator.Calculate - Average calculates average value.', function() {
        var result = Calculator.Average([1, 3, 5]);
        ok(result == 3);
    });

    test('Calculator.Calculate - Sum calculates sum value.', function() {
        var result = Calculator.Sum([1, 3, 4]);
        ok(result == 8);
    });

});