﻿var Calculation = (function () {
    return function (operator, operandSet) {
        return {
            Operator: operator || '',
            OperandSet: operandSet || []
        };
    };
})();